﻿using UnityEngine;
using System.Collections;

public class GenerateCountingQuestion : MonoBehaviour
{
    private int numberOfObject;
    private int indexOfSprite;
    public Sprite[] objects;
    public Sprite[] hiddenObjects;
    public UI2DSprite objectSprite;
    public UILabel questionLabel;
    public UILabel countedObjectLabel;
    public GameObject hiddenPrefab;
    public GameObject countedObjectsGrid;

	// Use this for initialization
	void Start ()
    {
        indexOfSprite = Random.Range(0, 2);

        if(Gamepublic.selectedLvl < 6)
        {
            numberOfObject = Random.Range(1, 5);
        }
        else
        {
            numberOfObject = Random.Range(1, 10);
        }
       
        questionLabel.text = numberOfObject.ToString();
        //countedObjectLabel.text = 0;

        switch(indexOfSprite)
        {
            case 1:
                {
                    objectSprite.sprite2D = objects[indexOfSprite];
                    break;
                }
            case 2:
                {
                    objectSprite.sprite2D = objects[indexOfSprite];
                    break;
                }
            default:
                {
                    objectSprite.sprite2D = objects[indexOfSprite];
                    break;
                }
        }

        GenerateCountedObjectsPanel();
	}

    void GenerateCountedObjectsPanel()
    {
        for(int i = 0; i < numberOfObject; ++i)
        {
            GameObject child = (GameObject)Instantiate(hiddenPrefab);
            child.transform.SetParent(countedObjectsGrid.transform);
            child.transform.localPosition = Vector3.zero;
            child.transform.localScale = Vector3.one;
            child.GetComponent<UI2DSprite>().sprite2D = hiddenObjects[indexOfSprite];
        }

        countedObjectsGrid.GetComponent<UIGrid>().Reposition();
    }

}
