﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class WriteController : MonoBehaviour
{
    [SerializeField]private GameObject pausePanel;
    [SerializeField]private GameObject winPanel;
    [SerializeField]private GameObject losePanel;
    [SerializeField]private GameObject pencil;
    [SerializeField]private GameObject tutorialPanel;
    private VectorLine line;
    private bool isMouseDown;
    private Vector3 mousePos;
    private List<Vector3> pointsList;
    private int nLinePoints;
    private int life;
    [SerializeField]private UI2DSprite[] stars;
    [SerializeField]private GameObject[] hearts;
    [SerializeField]private Sprite sprStar;
    [SerializeField]private GameObject starEffectPrefab;
    private TracingPath tracingScript;
    private bool isSkip;
    private int index;
    private bool isPause;
    private GameObject vectorCanvas;

    public Material lineMaterial;

    // Use this for initialization
    void Awake ()
    {
        index = 0;
        life = 3;
        nLinePoints = 0;
        isPause = false;
        pointsList = new List<Vector3>();
        isMouseDown = false;
        isSkip = false;
    }

    void Start()
    {
        tracingScript = GameObject.FindGameObjectWithTag("TracingPath").GetComponent<TracingPath>();
        AudioController.instance.PlayWriteBackgroundMusic();
        StartCoroutine(GameLoop());
    }

    public void PauseButtonPressed()
    {
        AudioController.instance.PlayButtonMusic();
        if (line != null)
        {
            line.active = false;
        }
        isPause = true;
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void ResumeButton_Click()
    {
        isPause = false;
        Time.timeScale = 1;
        AudioController.instance.PlayButtonMusic();
        if (line != null)
        {
            line.active = true;
        }
        pausePanel.SetActive(false);
    }

    public void HomeButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        Gamepublic.isFirstRun = false;
        Gamepublic.selectedLvl = 0;
        SceneManager.LoadScene(0);
    }

    public void RestartButton_Click()
    {
        Time.timeScale = 1;
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PlayNextButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        if (Gamepublic.selectedLvl == 10)
        {
            Gamepublic.selectedLvl = 0;
            //Load scene chu so
        }
        else
        {
            Gamepublic.selectedLvl++;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void SkipTutorial()
    {
        AudioController.instance.PlayButtonMusic();
        tutorialPanel.SetActive(false);
        isSkip = true;
        //Destroy(tutorialPanel);
    }

    IEnumerator GameLoop()
    {
        if (Gamepublic.selectedLvl == 1 && !PlayerPrefs.HasKey("drawN" + 1))
        {
            AudioController.instance.PlayDrawNumberIntro();
            tutorialPanel.SetActive(true);
        }
        else
        {
            Time.timeScale = 1.0f;
            isSkip = true;
            Destroy(tutorialPanel);
        }

        Debug.Log("isSKip: " + isSkip);
        //yield return new WaitUntil(() => isSkip == true);

        while (true)
        {
            RaycastHit2D hit2d = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            Debug.Log("tag: " + hit2d.collider.tag);
            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
            {
                if (hit2d.collider.tag.Equals("PauseButton"))
                {
                    this.PauseButtonPressed();
                }
                else
                {
                    if (hit2d.collider.tag.Equals("Number") || hit2d.collider.tag.Equals("Point") || hit2d.collider.tag.Equals("Untagged"))
                    {
                        line = new VectorLine("Line", new List<Vector3>(), 30.0f, LineType.Continuous, Joins.Fill);
                        line.collider = true;
                        line.trigger = true;
                        line.material = lineMaterial;
                        isMouseDown = true;
                    }
                    else
                    {
                        if (hit2d.collider.tag.Equals("Ground"))
                        {
                            life--;
                            if (life >= 0)
                            {
                                hearts[life].SetActive(false);
                            }
                        }
                    }
                }
            }

            if (isMouseDown)
            {

                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = 0;
                if (!pointsList.Contains(mousePos))
                {
                    pointsList.Add(mousePos);
                    line.points3.Add(mousePos);
                    //nLinePoints = line.points3.Count;
                    line.Draw();
                }
            }

            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
            {
                if (isMouseDown && !isPause)
                {
                    if (vectorCanvas == null)
                    {
                        vectorCanvas = GameObject.Find("VectorCanvas");
                    }
                    isMouseDown = false;

                    if (!CheckLine())
                    {
                        AudioController.instance.PlayWriteNumberCorrectMusic();
                        //nLinePoints = line.points3.Count - 1;
                        if (isCompleteGame())
                        {
                            line.active = false;
                            WinGame();
                            yield break;
                        }
                    }
                    else
                    {
                        life--;
                        hearts[life].SetActive(false);
                    }
                }
            }

            if(pencil != null)
            {
                Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                clickPosition.z = -6;
                pencil.transform.position = new Vector3(clickPosition.x + 0.08f, clickPosition.y + 0.08f, clickPosition.z);
            }

            if (isGameOver())
            {
                GameOver();
                yield break;
            }

            yield return new WaitForSeconds(0.01f);
        }
    }

    //Check if win game
    private bool isCompleteGame()
    {
        foreach (TracingPoint point in tracingScript.Points)
        {
            if (point.IsCheck != true)
            {
                return false;
            }
        }
        return true;
    }

    //Check if line completed
    private bool CheckLine()
    {
        int curLinePoints = line.points3.Count;
        if (!tracingScript.Points[index].IsCheck || !tracingScript.Points[index + 1].IsCheck)
        {
            line.points3.RemoveRange(nLinePoints, curLinePoints - nLinePoints - 1);
            line.Draw();
            VectorLine.Destroy(ref line);
            return true; // line not reached next tracing point
        }

        index += 2;

        return false; // Line reached tracing points
    }

    private void WinGame()
    {
        AudioController.instance.PlayWriteNumberRightMusic();
        PlayerPrefs.SetInt("drawN" + Gamepublic.selectedLvl, life);
        vectorCanvas.SetActive(false);
        StartCoroutine(StarAnimation());
        StartCoroutine(StarEffect());
        winPanel.SetActive(true);
    }

    //Check if game over
    private bool isGameOver()
    {
        if (life <= 0)
        {
            Debug.Log("GameOver");
            if (line != null)
            {
                line.active = false;
            }
            return true;
        }
        return false;
    }

    private void GameOver()
    {
        AudioController.instance.PlayLoseMusic();
        vectorCanvas.SetActive(false);
        losePanel.SetActive(true);
    }

    IEnumerator StarAnimation()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < life; ++i)
        {
            stars[i].sprite2D = sprStar;

            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }

    IEnumerator StarEffect()
    {
        int width = Screen.width - 50;
        int height = Screen.height - 50;
        while (true)
        {
            GameObject effect = (GameObject)Instantiate(starEffectPrefab);
            effect.transform.SetParent(winPanel.transform);
            effect.transform.localPosition = new Vector3(Random.Range(-width, width), Random.Range(-height, height), 0);
            effect.transform.localScale = new Vector3(2.5f, 2.5f, 1.0f);

            yield return new WaitForSeconds(0.25f);
        }
        //yield break;
    }
}
