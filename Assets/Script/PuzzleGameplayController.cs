﻿using UnityEngine;
using System.Collections;

public class PuzzleGameplayController : MonoBehaviour {


	[SerializeField] GameObject heartBar;
	private int heart;
	private int quantumPuzzle;
	private GameObject puzzle;

	void Start () {
		heart = 3;
	}

	public void StartGame(){
		InstancePuzzle ();
	}

	void InstancePuzzle(){
		puzzle = Instantiate(Resources.Load("Prefabs/PuzzleGame/Puzzle"+Gamepublic.selectedLvl.ToString ()) as GameObject);
		puzzle.transform.SetParent(transform);
		puzzle.transform.localScale = Vector3.one;
		puzzle.transform.localPosition = Vector3.zero;

		quantumPuzzle = puzzle.transform.GetChild (0).transform.childCount;
		Debug.Log (quantumPuzzle);
	}

	public void PauseGame (){
		Time.timeScale = 0;
		GameObject pausePanel = Instantiate(Resources.Load("Prefabs/PuzzleGame/PausePanel") as GameObject);
		pausePanel.transform.SetParent(transform.parent);
		pausePanel.transform.localScale = Vector3.one;
		pausePanel.transform.localPosition = Vector3.zero;
	}

	public void WrongMatch(){
		heart -= 1;
		NGUITools.Destroy (heartBar.transform.GetChild (heart).gameObject);
		if(heart == 0){
			ShowGameoverPanel ();
		}
	}

	public void CorrectMatch(){
		quantumPuzzle -= 1;
		if (quantumPuzzle == 0)
			ShowGameoverPanel ();
	}

	public void ShowGameoverPanel(){
		if(heart >=1 ){
			if(PlayerPrefs.HasKey("puzzle" + Gamepublic.selectedLvl.ToString ()) == true){
				int currentHeart = PlayerPrefs.GetInt ("cal" + Gamepublic.selectedLvl.ToString (), heart);
				if(heart > currentHeart)
					PlayerPrefs.SetInt ("puzzle" + Gamepublic.selectedLvl.ToString (), heart);
			}
			else
				PlayerPrefs.SetInt ("puzzle" + Gamepublic.selectedLvl.ToString (), heart);
		}
		GameObject gameoverPanel = Instantiate(Resources.Load("Prefabs/PuzzleGame/GameoverPanel") as GameObject);
		gameoverPanel.transform.SetParent(transform.parent);
		gameoverPanel.transform.localScale = Vector3.one;
		gameoverPanel.transform.localPosition = Vector3.zero;
		gameoverPanel.GetComponent<GameOverController>().RemainHeart = heart;

		NGUITools.Destroy (puzzle);
	}

	void OnEnable()
	{
		DragDropController.onWrongMatch += WrongMatch;
		DragDropController.onCorrectMatch += CorrectMatch;

	}
	void OnDisable()
	{
		DragDropController.onWrongMatch -= WrongMatch;
		DragDropController.onCorrectMatch += CorrectMatch;
	}
}
