﻿using UnityEngine;
using System.Collections;
using CalculatorDelegate;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml.Linq;

public class GameplayController : MonoBehaviour {

	public static event ChangPosistionShip onChangPosistionShip;

	private const int numberQuestion = 10;
	private int countAnswered = 0;
	private int countRightAnswered  = 0;
//	private List<string> wrongAnsewred = new List<string> ();
	private float[] posAsteriod = new float[3];
	private int heart;

	private int maxNumberToQuestion;
	private int fistNumber;
	private int secondNumber;
	private int resultOftQuestion;

	private string answerOfPlayer;

	[SerializeField] GameObject heartBar;
	[SerializeField] GameObject spawnPoint;
	[SerializeField] UILabel lblQuestion;
	[SerializeField] MainController ship;
	[SerializeField] Transform answerTrans;
	[SerializeField] GameObject answerBox;



	public int RightAnswerd {
		get{
			return countRightAnswered;
		}
	}

	public int Answered{
		get{
			return countAnswered;
		}
	}

	public int Heart{
		get{ 
			return heart;
		}
	}

	// Use this for initialization
	void Start () {
		posAsteriod [0] = 180f;
		posAsteriod [1] = 70f;
		posAsteriod [2] = -100f;

		heart = 3;

		lblQuestion.text = "0/" + numberQuestion.ToString ();

		maxNumberToQuestion = Gamepublic.selectedLvl * 5;
		LoadAnswerButton ();

	}

	void LoadAnswerButton(){
		for(int i =0; i <= 10; i++){
			GameObject child = Instantiate(Resources.Load("Prefabs/btnAnswer") as GameObject);
			child.transform.SetParent(answerTrans);
			child.transform.localScale = Vector3.one;
			child.transform.localPosition = Vector3.zero;
			if (i == 10) {
				child.GetComponentInChildren <UILabel> ().text = "X";
				child.name = "X";
			}
			else{
				child.GetComponentInChildren <UILabel> ().text = i.ToString ();
				child.name = i.ToString ();
			}


			EventDelegate.Set (child.GetComponent<UIButton>().onClick, Answer);
		}
		answerTrans.GetComponent<UIGrid>().enabled = true;
	}

	public void Answer(){
		string answer = UIButton.current.name;
		answerBox.SetActive (true);
		if (answer != "X" && answer.Length <= 2) {
			answerOfPlayer += answer;
			int temp;
			if(int.TryParse (answerOfPlayer, out temp))
				CheckAnswer (int.Parse (answerOfPlayer));

		}	
		else{
			answerOfPlayer = null;
			answerBox.GetComponentInChildren<UILabel> ().text = answerOfPlayer;
		}
	}

	void CheckAnswer(int answer){
		if(answer <= 100){
			answerBox.GetComponentInChildren<UILabel>().text = answer.ToString ();
			if(answer == resultOftQuestion){
				answerOfPlayer = null;
				resultOftQuestion = 0;
				countRightAnswered += 1;
				//NGUITools.DestroyChildren (spawnPoint.transform);
				if (onChangPosistionShip != null){
					onChangPosistionShip(true);
				}
				if(countAnswered == 10){
					ShowGameoverPanel ();
				}
			}
		}
	} 

	public void StartGame(){
		InvokeRepeating ("SpawnBlock", 0f, 5f);
	}

	void SpawnBlock(){
		countAnswered += 1;
		lblQuestion.text = countAnswered.ToString () +"/" + numberQuestion.ToString ();

		GameObject child = Instantiate(Resources.Load("Prefabs/block") as GameObject);
		child.transform.SetParent(spawnPoint.transform);
		child.transform.localScale = Vector3.one;
		child.transform.localPosition = spawnPoint.transform.position + new Vector3 (0f, posAsteriod[ship.PosMain], 0f);
		child.GetComponent<Rigidbody2D> ().velocity = new Vector3 (-0.5f, 0, 0);
		child.GetComponentInChildren<UILabel> ().text = initQuestion ();
		string indexAtlas = Random.Range (1, 5).ToString ();
		if(indexAtlas.Equals ("1") == false){
			GameObject temp = Resources.Load ("UIAtlas/Alien"+indexAtlas) as GameObject;
			child.GetComponent<UISprite> ().atlas.replacement = temp.GetComponent<UIAtlas> ();
		}
		answerBox.SetActive (false);
	}

	string  initQuestion(){

		fistNumber = Random.Range (maxNumberToQuestion - 5, maxNumberToQuestion);
		secondNumber = Random.Range (maxNumberToQuestion - 5, maxNumberToQuestion);
		string question;
		if (Random.Range (1, 3) == 2){
			resultOftQuestion = fistNumber + secondNumber;
			question = fistNumber.ToString () + " + " + secondNumber.ToString () + " = ?";
		}	
		else{
			if(fistNumber >= secondNumber){
				resultOftQuestion = fistNumber - secondNumber;
				question = fistNumber.ToString () + " - " + secondNumber.ToString () + " = ?";
			}
			else{
				resultOftQuestion = secondNumber - fistNumber;
				question = secondNumber.ToString () + " - " + fistNumber.ToString () + " = ?";
			}
		}
		return question;
	}

	public void WrongAnswer(){
		heart -= 1;
		//wrongAnsewred.Add (wrongAnswer);
		answerBox.SetActive (false);
		answerOfPlayer = null;
		NGUITools.Destroy (heartBar.transform.GetChild (heart).gameObject);
		NGUITools.DestroyChildren (spawnPoint.transform);
		if(heart == 0){
			ShowGameoverPanel ();
		}
	}


	public void PauseGame(){
		Time.timeScale = 0;
		GameObject pausePanel = Instantiate(Resources.Load("Prefabs/PausePanel") as GameObject);
		pausePanel.transform.SetParent(transform.parent);
		pausePanel.transform.localScale = Vector3.one;
		pausePanel.transform.localPosition = Vector3.zero;
	}
//		
//	public void CheckRightAnswer(bool isRight, string wrongAnswer){
//		if(isRight){
//			countRightAnswered += 1;
//		}else{
//			heart -= 1;
//			wrongAnsewred.Add (wrongAnswer);
//			NGUITools.Destroy (heartBar.transform.GetChild (heart).gameObject);
//			NGUITools.DestroyChildren (spawnPoint.transform);
//			Debug.Log ("Animation Explosion"); 
//			if(heart == 0){
//				ShowGameoverPanel ();
//				return;
//			}
//		}
//
//		if(countAnswered == 10){
//			ShowGameoverPanel ();
//		}
//	}

	public void ShowGameoverPanel(){
		CancelInvoke ();
		if(heart >=1 ){
			if(PlayerPrefs.HasKey("cal" + Gamepublic.selectedLvl.ToString ()) == true){
				int currentHeart = PlayerPrefs.GetInt ("cal" + Gamepublic.selectedLvl.ToString (), heart);
				if(heart > currentHeart)
					PlayerPrefs.SetInt ("cal" + Gamepublic.selectedLvl.ToString (), heart);
			}
			else
				PlayerPrefs.SetInt ("cal" + Gamepublic.selectedLvl.ToString (), heart);
		}
		GameObject gameoverPanel = Instantiate(Resources.Load("Prefabs/GameoverPanel") as GameObject);
		gameoverPanel.transform.SetParent(transform.parent);
		gameoverPanel.transform.localScale = Vector3.one;
		gameoverPanel.transform.localPosition = Vector3.zero;
		gameoverPanel.GetComponent<GameOverController>().RightAnswered = RightAnswerd;
		NGUITools.Destroy (this.gameObject);
	}

}
