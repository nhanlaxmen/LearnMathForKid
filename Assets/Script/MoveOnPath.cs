﻿using UnityEngine;
using System.Collections;

public class MoveOnPath : MonoBehaviour
{
    [SerializeField]private Transform[] points;

	// Use this for initialization
	void Start ()
    {
        iTween.MoveTo(gameObject, iTween.Hash("speed", 0.5, "path", points, "looptype", iTween.LoopType.loop, "easetype", iTween.EaseType.linear, "movetopath", false));
	}
}
