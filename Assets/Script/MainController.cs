﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class MainController : MonoBehaviour {

	private enum POS {Top = 0, Mid = 1, Bot = 2};
	[SerializeField] private GameplayController gameplayController;
	private  Vector3 Pos;

	private int posMain;

	public int PosMain{
		set{
			posMain = value;
		}
		get{
			return posMain;
		}
	}
	// Use this for initialization
	void Start () {
		posMain = (int) POS.Mid;
		Pos = new Vector3 (0f, 110f, 0f);
	} 

	void OnEnable()
	{
		GameplayController.onChangPosistionShip += ChangePosShip;

	}
	void OnDisable()
	{
		GameplayController.onChangPosistionShip -= ChangePosShip;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "block"){
			Debug.Log ("AAAAAA");
			GameObject Explosion = Instantiate(Resources.Load("Prefabs/CountingNumberScene/Explosion") as GameObject);
			Explosion.transform.SetParent (transform);
			Explosion.transform.localPosition = other.transform.localPosition;
			Explosion.transform.localScale = Vector3.one;
			gameplayController.WrongAnswer ();
		}
	}

	public void ChangePosShip(bool isRight){
		if (isRight){
			switch(posMain){
			case 0:
				posMain = (int)POS.Mid;
				iTween.MoveTo (gameObject, iTween.Hash ("y",0.15f,"time",2f));
				//transform.localPosition -= Pos;
				break;
			case 1:
				if (Random.Range (0, 3) != 2){
					posMain = (int)POS.Top;
					iTween.MoveTo (gameObject, iTween.Hash ("y",+ 0.5f,"time",2f));
					//transform.localPosition += Pos;

				}
				else{
					posMain = (int)POS.Bot;
					iTween.MoveTo (gameObject, iTween.Hash ("y",- 0.4f,"time",2f));
					//transform.localPosition -= Pos;

				}
					
				break;
			case 2:
				posMain = (int)POS.Mid;
				iTween.MoveTo (gameObject, iTween.Hash ("y",0.15f,"time",2f));
				//transform.localPosition += Pos;
				break;
			}
		}
	}

}
