﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class GameOverController : MonoBehaviour {
	
	[SerializeField] private UILabel Result ;
	[SerializeField] private UISprite[] Star;
	[SerializeField] private UILabel Notice;
	[SerializeField] private UIButton NextPlay;
	private int rightAnswered;
	private String starSpriteName = "btnStar1";
	string namePrefabs;
	int indexScene;

	private int remainHeart;
	public int RightAnswered{
		set{
			rightAnswered = value;
		}
	}

	public int RemainHeart{
		set{
			remainHeart = value;
		}
	}

	void Start () {
		ShowInforGameover ();
	}

	void ShowInforGameover(){
		indexScene = SceneManager.GetActiveScene().buildIndex;
		switch(indexScene)
		{
		case 5:
			{
				namePrefabs = "PuzzleGame/PuzzleGameplay";
				showStarOfPuzzle ();
				break;
			}
		default:
			{
				namePrefabs = "GameplayPanel";
				showInforAndStarOfCalculator ();
				break;
			}
		}
	}

	void showStarOfPuzzle(){
		switch(remainHeart){
			case 1:
				Star[1].spriteName = starSpriteName;
				Notice.text = "Khá  Lắm";
				break;
			case 2:
				Star[0].spriteName = starSpriteName;
				Star[2].spriteName = starSpriteName;
				Notice.text = "Tốt  Lắm";
				break;
			case 3:
				Notice.text = "Giỏi  Lắm ";
				for (int i = 0; i < 3; i++)
					Star [i].spriteName = starSpriteName;
				break;
			default:
				Notice.text = "Thử  Lại  Nha";
				NextPlay.normalSprite2D = NextPlay.disabledSprite2D;
				NextPlay.enabled = false;
				break;
		}
	}

	void showInforAndStarOfCalculator(){
		
		Result.text ="Số  Câu  Đúng: " + rightAnswered.ToString () + " /10";
		switch(rightAnswered){
			case 8:
				Star[1].spriteName = starSpriteName;
				Notice.text = "Khá  Lắm";
				break;
			case 9:
				Star[0].spriteName = starSpriteName;
				Star[2].spriteName = starSpriteName;
				Notice.text = "Tốt  Lắm";
				break;
			case 10:
				Notice.text = "Giỏi  Lắm ";
				for (int i = 0; i < 3; i++)
					Star [i].spriteName = starSpriteName;
				break;
			default:
				Notice.text = "Thử  Lại  Nha";
				NextPlay.normalSprite2D = NextPlay.disabledSprite2D;
				NextPlay.enabled = false;
				break;
		}
	}
	
	public void Play(){
		//SceneManager.LoadScene("Calculator");
		Gamepublic.selectedLvl += 1;
		startGame ();
	}

	public void Restart(){
		startGame ();
	}

	public void Home(){
		SceneManager.LoadScene (0);
	}

	void startGame(){
		GameObject gameplayPanel = Instantiate(Resources.Load("Prefabs/"+namePrefabs) as GameObject);
		gameplayPanel.transform.SetParent(transform.parent);
		gameplayPanel.transform.localScale = Vector3.one;
		gameplayPanel.transform.localPosition = Vector3.zero;
		if(indexScene != 5)
			gameplayPanel.GetComponent <GameplayController>().StartGame ();
		else
			gameplayPanel.GetComponent <PuzzleGameplayController>().StartGame ();
		NGUITools.Destroy (this.gameObject);
	}
}
