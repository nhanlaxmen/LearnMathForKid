﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TracingPath : MonoBehaviour
{
    [SerializeField]private List<TracingPoint> points;

    public List<TracingPoint> Points
    {
        get
        {
            return points;
        }
        set
        {
            points = value;
        }
    }

	// Use this for initialization
	void Awake ()
    {
        points = new List<TracingPoint>();
        int length = gameObject.transform.childCount;

        for(int i = 0; i < length; i++)
        {
            points.Add(gameObject.transform.GetChild(i).GetComponent<TracingPoint>());
        }
	}

    //Get number of checked points
    public int GetCheckedPoints()
    {
        int total = 0;

        for(int i = 0; i < points.Count; i++)
        {
            if(points[i].IsCheck)
            {
                total++;
            }
        }

        return total;
    }

    public float GetDistance(int start, int dest)
    {
        float distance = 0;

        distance = Vector3.Distance(points[start].gameObject.transform.position, points[dest].gameObject.transform.position); 

        return distance;
    }
}
