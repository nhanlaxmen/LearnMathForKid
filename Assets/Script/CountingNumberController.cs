﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CountingNumberController : MonoBehaviour
{
    [SerializeField]private Transform spawnLocation;
    [SerializeField]private Transform missileSpawnLocation;
    [SerializeField]private GameObject questionPanel;
    [SerializeField]private Sprite sprStar;
    [SerializeField]private GameObject starEffectPrefab;
    [SerializeField]private GameObject tutorialPanel;
    private float spawnStart;
    private float spawnWait;
    private float startGameWait;
    private bool isGameOver;
    private bool hasWon;
    private bool isSkip;
    private int life;

    public GameObject[] obstacles;
    public UI2DSprite[] stars;
    public GameObject gameOverPanel;
    public GameObject completeGamePanel;
    public GameObject pausePanel;
    public UILabel questionLabel;
    public int questionNumber;
    //public GameObject Camera;

    // Use this for initialization
    void Start ()
    {
        isGameOver = false;
        hasWon = false;
        isSkip = false;
        life = 3;
        spawnStart = 0.5f;
        spawnWait = 2.0f;
        startGameWait = 1.0f;
        //obstacles = new GameObject[3];
        //spawnLocation = GameObject.FindGameObjectWithTag("SpawnLocation").transform;
        StartCoroutine(GameLoop());
	}

    
    IEnumerator GameLoop()
    {
        AudioController.instance.PlayCountBackgroundMusic();
        if(Gamepublic.selectedLvl != 1)
        {
            Destroy(tutorialPanel);
            //isSkip = true;
        }
        yield return new WaitForSeconds(startGameWait);

        //Generate question
        questionPanel.SetActive(true);

        if (Gamepublic.selectedLvl == 1 && !PlayerPrefs.HasKey("count" + 1))
        {
            AudioController.instance.PlayCountingIntro();
            tutorialPanel.SetActive(true);
        }
        else
        {
            Time.timeScale = 1.0f;
        }

        //yield return new WaitUntil(() => isSkip == true);

        yield return new WaitForSeconds(spawnStart);
        questionNumber = int.Parse(questionLabel.text);



        //Game loop: spawn obstacle every <spawnWait> seconds
        while (true)
        {
            
            //GameObject obstacle = obstacles[Random.Range(0, obstacles.Length)];
            //Vector3 spawnPosition = new Vector3(Random.Range(0, spawnLocation.localPosition.x), spawnLocation.localPosition.y, spawnLocation.localPosition.z);
            //Debug.Log("x:" + spawnPosition.x + "; y:" + spawnPosition.y + "; z:" + spawnPosition.z);
            //Quaternion spawnRotation = Quaternion.identity;
            ////obstacle.transform.SetParent(Camera.transform);
            //Instantiate(obstacle, spawnPosition, spawnRotation);
            SpawnObstacle();

            yield return new WaitForSeconds(spawnWait);

            //end game loop
            if(isGameOver || hasWon)
            {
                yield break;
            }
        }
    }

    void SpawnObstacle()
    {
        GameObject child = (GameObject)Instantiate(obstacles[Random.Range(0, obstacles.Length)]);
        child.transform.SetParent(spawnLocation);
        child.transform.localScale = Vector3.one;
        child.transform.localPosition = new Vector3(Random.Range(-spawnLocation.localPosition.x, spawnLocation.localPosition.x - 50), spawnLocation.position.y, spawnLocation.localPosition.z);
        child.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -0.25f, 0);
        child.GetComponent<MissileController>().MissileSpawnLocation = missileSpawnLocation;
    }

    public void GameOver()
    {
        AudioController.instance.PlayLoseMusic();
        isGameOver = true;
        gameOverPanel.SetActive(true);
    }

    public void WinGame()
    {
        AudioController.instance.PlayWinMusic();
        PlayerPrefs.SetInt("count" + Gamepublic.selectedLvl, life);
        StartCoroutine(StarAnimation());
        StartCoroutine(StarEffect());
        hasWon = true;
        completeGamePanel.SetActive(true);
    }

    public int GetLife()
    {
        return life;
    }

    public void SetLife(int life)
    {
        this.life = life;
    }

    public void HomeButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        Gamepublic.isFirstRun = false;
        Gamepublic.selectedLvl = 0;
        SceneManager.LoadScene(0);
    } 

    public void RestartButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PlayNextButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        AudioController.instance.StopAudio();
        if (Gamepublic.selectedLvl == 10)
        {
            //Load scene Ve hinh
        }
        else
        {
            Gamepublic.selectedLvl++;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void PauseButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void ResumeButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void SkipTutorial()
    {
        AudioController.instance.PlayButtonMusic();
        tutorialPanel.SetActive(false);
        isSkip = true;
        Destroy(tutorialPanel);
    }

    IEnumerator StarAnimation()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < life; ++i)
        {
            stars[i].sprite2D = sprStar;

            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }

    IEnumerator StarEffect()
    {
        int width = Screen.width - 50;
        int height = Screen.height - 50;
        Debug.Log("screen width: " + Screen.width);
        Debug.Log("screen height: " + Screen.height);
        while(true)
        {
            if (!AudioController.instance.IsPlaying())
            {
                AudioController.instance.PlayFireworkMusic();
            }
            GameObject effect = (GameObject)Instantiate(starEffectPrefab);
            effect.transform.SetParent(completeGamePanel.transform);
            effect.transform.localPosition = new Vector3(Random.Range(-(width / 2), (width / 2)), Random.Range(-(height / 2), (height / 2)), 0);
            effect.transform.localScale = new Vector3(2.5f, 2.5f, 1.0f);
            yield return new WaitForSeconds(1f);
        }
        //yield break;
    }


}
