﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyOnContact : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> listOfObjectsInGrid;
    [SerializeField]
	private UISprite collidedObjectScript;
    private int life;
    [SerializeField]
    private CountingNumberController gameController;
    private UI2DSprite questionObjectScript;
    [SerializeField]
    private GameObject countedObjectsGrid;
    private GameObject[] heartSprites;
    [SerializeField]private UILabel questionLabel;

    public GameObject explosionPrefab;
    public GameObject starEffectPrefab;
    //public UISprite sprite;

	void Awake(){


	}


    void Start()
    {     
        questionObjectScript = GameObject.FindGameObjectWithTag("QuestionSprite").GetComponent<UI2DSprite>();
        countedObjectsGrid = GameObject.FindGameObjectWithTag("CountedObjectsGrid");
        questionLabel = GameObject.FindGameObjectWithTag("QuestionLabel").GetComponent<UILabel>();
        if (countedObjectsGrid == null)
        {
            Debug.Log("Can't find countedObjectsGrid");
        }
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<CountingNumberController>();
        life = gameController.GetLife();
        heartSprites = new GameObject[3];
        listOfObjectsInGrid = new List<GameObject>();

        GetHeart();
        GetObjectInGrid();
    }

    //public GameObject explosion;
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag.Equals("block"))
        {
			collidedObjectScript = other.GetComponent<UISprite>();
            //Vector3 offset = new Vector3((other.gameObject.transform.localPosition.x + gameObject.transform.localPosition.x)/2, (other.gameObject.transform.localPosition.y + gameObject.transform.localPosition.y)/2, 0);
            //Tao animation no
            GameObject explosion = (GameObject)Instantiate(explosionPrefab);
            explosion.transform.SetParent(GameObject.FindGameObjectWithTag("Camera").transform);
            //explosion.transform.localPosition = offset;
            explosion.transform.localPosition = new Vector3(gameObject.transform.localPosition.x + 25, gameObject.transform.localPosition.y, 0);
            explosion.transform.localScale = Vector3.one;

            string name = collidedObjectScript.spriteName;
            Debug.Log(name);
            //Change sprite when user tap in correct object
            if (name.Equals(questionObjectScript.sprite2D.name))
            {
                AudioController.instance.PlayRightAnswerMusic();
                Debug.Log("number of question: " + gameController.questionNumber);
                gameController.questionNumber++;
                Debug.Log("number of question after plus: " + gameController.questionNumber);
                gameController.questionLabel.text = gameController.questionNumber.ToString();
                
                for (int i = 0; i < listOfObjectsInGrid.Count; ++i)
                {
                    if (listOfObjectsInGrid[i].GetComponent<UI2DSprite>().sprite2D.name != name)
                    {
                        GameObject effect = (GameObject)Instantiate(starEffectPrefab);
                        effect.transform.SetParent(GameObject.FindGameObjectWithTag("Camera").transform);
                        effect.transform.position = listOfObjectsInGrid[i].transform.position;
                        Debug.Log("effect pos: " + effect.transform.localPosition.x + " " + effect.transform.localPosition.y);
                        effect.transform.localScale = Vector3.one;
                        listOfObjectsInGrid[i].GetComponent<UI2DSprite>().sprite2D = questionObjectScript.sprite2D;
                        break;
                    }
                }
                if (questionLabel.text.Equals(gameController.questionNumber.ToString()))
                {
                    gameController.WinGame();
                }
            }
            //Decrease life if tap wrong object
            else
            {
                AudioController.instance.PlayWrongAnswerMusic();
                life--;
                gameController.SetLife(life);
                heartSprites[life].SetActive(false);
                if (life == 0)
                {
                    gameController.GameOver();
                }
            }

            NGUITools.Destroy(other.gameObject);
            NGUITools.Destroy(gameObject);
        }
    }

    //Get all child objects in grid
    void GetObjectInGrid()
    {
        int numberOfObject = countedObjectsGrid.transform.childCount;
        for (int i = 0; i < numberOfObject; ++i)
        {
            listOfObjectsInGrid.Add(countedObjectsGrid.transform.GetChild(i).gameObject);
        }
    }

    void GetHeart()
    {
        GameObject heartBar = GameObject.FindGameObjectWithTag("Hearts");
        for(int i = 0; i < heartBar.transform.childCount; ++i)
        {
            heartSprites[i] = heartBar.transform.GetChild(i).gameObject;
        }
    }
}
