﻿using UnityEngine;
using System.Collections;
using System;

namespace CalculatorDelegate {
	public delegate void CheckRightAnswer(bool isRight, string wrongAnswer); // event in class QuestionController. Catch event in class GameplayController
	public delegate void ChangPosistionShip (bool isRight); // event in class QuestionController. Catch event in class MainController

	public delegate void WrongMatch();
	public delegate void CorrectMatch();
}
