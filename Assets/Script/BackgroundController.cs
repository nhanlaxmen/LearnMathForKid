﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class BackgroundController : MonoBehaviour {

	private float speedScorllBG = -0.75f;
	public Vector3 positionNextBG = new Vector3 (1200f, 0f, 0f);
	void Start(){
		GetComponent<Rigidbody2D> ().velocity = new Vector3 (speedScorllBG, 0, 0);

	}

	void InstanceNextBackground(){
		GameObject nextBG = Instantiate (Resources.Load ("Prefabs/Background")) as GameObject;
		nextBG.transform.SetParent (transform.parent);
		nextBG.transform.localScale = Vector3.one;
		nextBG.transform.localPosition = positionNextBG;
        nextBG.GetComponent<UI2DSprite>().sprite2D = this.GetComponent<UI2DSprite>().sprite2D;
		if(this.GetComponent<UI2DSprite>().flip == UIBasicSprite.Flip.Nothing){
			nextBG.GetComponent<UI2DSprite>().flip = UIBasicSprite.Flip.Horizontally;
		}else nextBG.GetComponent<UI2DSprite>().flip = UIBasicSprite.Flip.Nothing;
	}

	void OnTriggerEnter2D(Collider2D other) {
		
		if(other.tag.Equals("rightLine")){
			InstanceNextBackground ();
		}
		if(other.tag == "leftLine"){
			NGUITools.Destroy (this.gameObject);
		}
	}
//	void Move(){
//		Debug.Log ("Move");
//		iTween.MoveTo (gameObject, iTween.Hash ("x",-1900f,"speed",700f,"islocal",true,"easetype",iTween.EaseType.linear,"onupdate","instanceNextBackground","onupdatetarget",gameObject,"oncomplete","autoDestroy","oncompletetarget",gameObject));
//	}
//
//	void autoDestroy(){
//		Debug.Log ("Destroy");
//		NGUITools.Destroy (this.gameObject);
//	}
//
//	void instanceNextBackground(){
//		Debug.Log ("next background");
//	}
}
