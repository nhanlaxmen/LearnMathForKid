﻿using UnityEngine;
using System.Collections;
using System;
using CalculatorDelegate;

public class DragDropController : UIDragDropItem {

	public static event WrongMatch onWrongMatch;
	public static event CorrectMatch onCorrectMatch;

	protected override void OnDragDropRelease(GameObject Surface){	
		if(Surface != null && Surface.name != "UI Root"){
			if(gameObject.name.Contains (Surface.name)){
				Surface.GetComponent<UI2DSprite>().sprite2D =  gameObject.GetComponent<UI2DSprite>().sprite2D;
				Surface.GetComponent<BoxCollider>().enabled = false;
				if (onCorrectMatch != null)
					onCorrectMatch ();
				NGUITools.Destroy (gameObject);
			}else{
				if (onWrongMatch != null)
					onWrongMatch ();
			}
		}

		base.OnDragDropRelease (Surface);
	}

	protected override void OnDragDropEnd(){
		base.OnDragDropEnd ();
	}
}
