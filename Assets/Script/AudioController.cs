﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour
{
    [SerializeField]private AudioSource audio;
    [SerializeField]private AudioSource backgroundAudio;
    [SerializeField]private AudioClip[] clips;
    public static AudioController instance;

    void Awake()
    {
        if(instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
        }
    }

 	// Use this for initialization
	void Start ()
    {
        backgroundAudio.loop = true;
        //audio = GetComponent<AudioSource>();
	}

    public bool IsPlaying()
    {
        return audio.isPlaying;
    }

    #region Intro music
    public void PlayMainMenuIntro()
    {
        audio.PlayOneShot(clips[0]);
    }

    public void PlayCalculateIntro()
    {
        audio.PlayOneShot(clips[1]);
    }

    public void PlayDrawImageIntro()
    {
        audio.PlayOneShot(clips[4]);
    }

    public void PlayDrawNumberIntro()
    {
        audio.PlayOneShot(clips[3]);
    }

    public void PlayCountingIntro()
    {
        audio.PlayOneShot(clips[2]);
    }
    #endregion

    #region Background music
    public void PlayMainMenuBackgroundMusic()
    {
        //backgroundAudio.PlayOneShot(clips[46]);
        backgroundAudio.clip = clips[46];
        backgroundAudio.Play();
    }

    public void PlayNumberBackgroundMusic()
    {
        //backgroundAudio.PlayOneShot(clips[45]);
        backgroundAudio.clip = clips[45];
        backgroundAudio.Play();
    }

    public void PlayCountBackgroundMusic()
    {
        //Play background scene chu so
        backgroundAudio.clip = clips[49];
        backgroundAudio.Play();
    }

    public void PlayWriteBackgroundMusic()
    {
        backgroundAudio.clip = clips[53];
        backgroundAudio.Play();
    }

    public void PlayDrawBackgroundMusic()
    {
        backgroundAudio.clip = clips[52];
        backgroundAudio.Play();
    }

    public void PlayCalculateBackgroundMusic()
    {
        backgroundAudio.clip = clips[51];
        backgroundAudio.Play();
    }

    public void PlayPuzzleBackgroundMusic()
    {
        backgroundAudio.clip = clips[50];
        backgroundAudio.Play();
    }
    #endregion

    #region Corrct answer music
    public void PlayRightAnswerMusic()
    {
        audio.PlayOneShot(clips[13]);
    }

    public void PlayDrawImageCorrectMusic()
    {
        audio.PlayOneShot(clips[10]);
    }

    //Play when player got a right answer
    public void PlayWriteNumberCorrectMusic()
    {
        audio.PlayOneShot(clips[7]);
    }

    //Play this sound clip when player got a right answer after an uncorrect answer
    public void PlayWriteNumberRightMusic()
    {
        audio.PlayOneShot(clips[8]);
    }
    #endregion

    #region Win music
    public void PlayWinMusic()
    {
        audio.PlayOneShot(clips[17]);
    }

    public void PlayWinDrawImageMusic()
    {
        audio.PlayOneShot(clips[10]);
    }

    public void PlayWinWriteNumberMusic()
    {
        audio.PlayOneShot(clips[47]);
    }
    #endregion

    #region Lost music
    public void PlayLoseMusic()
    {
        audio.PlayOneShot(clips[16]);
    }
    #endregion

    #region Replay music
    public void PlayReplayMusic()
    {
        audio.PlayOneShot(clips[15]);
    }
    #endregion

    #region Number music
    public void PlayNumberZero()
    {
        audio.PlayOneShot(clips[24]);
    }

    public void PlayNumberOne()
    {
        audio.PlayOneShot(clips[25]);
    }

    public void PlayNumberTwo()
    {
        audio.PlayOneShot(clips[26]);
    }

    public void PlayNumberThree()
    {
        audio.PlayOneShot(clips[27]);
    }

    public void PlayNumberFour()
    {
        audio.PlayOneShot(clips[28]);
    }

    public void PlayNumberFive()
    {
        audio.PlayOneShot(clips[29]);
    }

    public void PlayNumberSix()
    {
        audio.PlayOneShot(clips[30]);
    }

    public void PlayNumberSeven()
    {
        audio.PlayOneShot(clips[31]);
    }

    public void PlayNumberEight()
    {
        audio.PlayOneShot(clips[32]);
    }

    public void PlayNumberNine()
    {
        audio.PlayOneShot(clips[33]);
    }

    public void PlayNumberTen()
    {
        audio.PlayOneShot(clips[34]);
    }

    public void PlayNumberEleven()
    {
        audio.PlayOneShot(clips[35]);
    }

    public void PlayNumberTwelve()
    {
        audio.PlayOneShot(clips[36]);
    }

    public void PlayNumberThirteen()
    {
        audio.PlayOneShot(clips[37]);
    }

    public void PlayNumberFourteen()
    {
        audio.PlayOneShot(clips[38]);
    }

    public void PlayNumberFifteen()
    {
        audio.PlayOneShot(clips[39]);
    }

    public void PlayNumberSixteen()
    {
        audio.PlayOneShot(clips[40]);
    }

    public void PlayNumberSeventeen()
    {
        audio.PlayOneShot(clips[41]);
    }

    public void PlayNumberEighteen()
    {
        audio.PlayOneShot(clips[42]);
    }

    public void PlayNumberNineteen()
    {
        audio.PlayOneShot(clips[43]);
    }

    public void PlayNumberTwenty()
    {
        audio.PlayOneShot(clips[44]);
    }
    #endregion

    public void PlayMissleSound()
    {
        audio.PlayOneShot(clips[54]);
    }

    public void PlayWrongAnswerMusic()
    {
        audio.PlayOneShot(clips[14]);
    }

    public void PlayButtonMusic()
    {
        audio.PlayOneShot(clips[20]);
    }

    public void PlayFireworkMusic()
    {
        audio.PlayOneShot(clips[48]);
    }

    public void TurnOffSound()
    {
        audio.volume = 0.0f;
        backgroundAudio.volume = 0.0f;
    }

    public void TurnOnSound()
    {
        audio.volume = 1.0f;
        backgroundAudio.volume = 1.0f;
    }

    public void StopBackgroundMusic()
    {
        backgroundAudio.Stop();
    }

    public void StopAudio()
    {
        audio.Stop();
    }
}
