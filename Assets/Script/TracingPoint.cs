﻿using UnityEngine;
using System.Collections;

public class TracingPoint : MonoBehaviour
{
    [SerializeField]private bool isCheck;
    [SerializeField]private int index;
    private bool isTriggered;
    private Collider2D other;

    public bool IsCheck
    {
        get
        {
            return isCheck;
        }
    }

	// Use this for initialization
	void Start ()
    {
        isTriggered = false;
        isCheck = false;
        StartCoroutine(CheckExiting());
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name.Equals("Line"))
        {
            isTriggered = true;
            isCheck = true;
            this.other = other;
            Debug.Log("After enter: " + isCheck);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Check if Line");
        if (other.name.Equals("Line"))
        {
            isCheck = false;
            Debug.Log("After exit: " + isCheck);
        }
    }

    IEnumerator CheckExiting()
    {
        while(true)
        {
            if (isTriggered && !other && isCheck)
            {
                isCheck = false;
                Debug.Log("After exit: " + isCheck);
            }

            yield return new WaitForSeconds(0.02f);
        }
    }
}
