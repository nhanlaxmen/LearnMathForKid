﻿using UnityEngine;
using System.Collections;
using System.Security.AccessControl;
using UnityEngine.SceneManagement;

public class PausePanelController : MonoBehaviour {

	[SerializeField] private UILabel correctAnswer;
	[SerializeField] private UILabel ansewred;
	private GameplayController gameplayController;
	private int indexScene;

	// Use this for initialization
	void Start () {
		indexScene = SceneManager.GetActiveScene().buildIndex;
		if(indexScene == 1){
			gameplayController = transform.parent.GetComponentInChildren<GameplayController> ();
			correctAnswer.text = "Correct Answers: " + gameplayController.RightAnswerd.ToString ();
			ansewred.text = "Answered: " + gameplayController.Answered.ToString ();
		}

	}
	
	public void Continous(){

		Time.timeScale = 1;
		NGUITools.Destroy (gameObject);
	}

	public void Refresh(){
		Time.timeScale = 1;
		if (indexScene == 1)
			SceneManager.LoadScene (1);
		else 
			SceneManager.LoadScene (5);
	}

	public void Home(){
		Time.timeScale = 1;
		SceneManager.LoadScene (0);
	}
}
