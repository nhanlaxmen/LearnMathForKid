﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]private GameObject gameTitle;
    //[SerializeField]private GameObject startGameLabel;
    [SerializeField]private GameObject listFunction;
    [SerializeField]private GameObject btnStart;
    [SerializeField]private GameObject btnSound;
    [SerializeField]private GameObject btnInfo;
    //[SerializeField]private Transform settingPanelPosition;
    [SerializeField]private UISprite spriteScript;
    [SerializeField]private UI2DSprite sprMusicButton;
    private bool isStart;
    //private UI2DSprite btnSoundEffectScript;

    public Sprite sprDisableButton;
    public Sprite sprNormalButton;

	// Use this for initialization
	void Start ()
    {
        isStart = false;
        if(Gamepublic.isFirstRun)
        {
            listFunction.SetActive(false);
        }
        else
        {
            isStart = true;
            btnStart.SetActive(false);
            btnSound.SetActive(false);
            btnInfo.SetActive(false);
            gameTitle.SetActive(false);
            listFunction.SetActive(true);
        }

        //sprMusicButton = GameObject.FindGameObjectWithTag("MusicButton").GetComponent<UI2DSprite>();
        //btnSoundEffectScript = GameObject.FindGameObjectWithTag("SoundEffectButton").GetComponent<UI2DSprite>();
        StartCoroutine(StartButtonAnimation());
	}

    public void TapOnScreen()
    {
        AudioController.instance.PlayButtonMusic();
        if(!listFunction.activeSelf)
        {
            isStart = true;
            gameTitle.SetActive(false);
            //startGameLabel.SetActive(false);
            btnStart.SetActive(false);
            btnSound.SetActive(false);
            btnInfo.SetActive(false);
            listFunction.SetActive(true);
        }
    }

    IEnumerator StartButtonAnimation()
    {
        int index = 47;
        AudioController.instance.PlayMainMenuBackgroundMusic();
        while(!isStart)
        {   
            spriteScript.spriteName = "Button_" + index;
            if(index == 47)
            {
                index--;
            }
            else
            {
                index++;
            }
            yield return new WaitForSeconds(1.0f);
        }
        yield break;
    }

    #region Change scene methods
    public void ChangeToWriteNumberScene()
    {
        //Load WriteNumberScene
        AudioController.instance.PlayButtonMusic();
        SceneManager.LoadScene(3);
    }

    public void ChangeToCountingScene()
    {
        //Load CountingScene
        AudioController.instance.PlayButtonMusic();
        SceneManager.LoadScene(2);
    }

    public void ChangeToCompareScene()
    {
        //Load CompareScene
        //SceneManager.LoadScene(1);
    }

    public void ChangeToSortingScene()
    {
        //Load SortingScene
        //SceneManager.LoadScene(3);
    }

    public void ChangeToDrawingScene()
    {
        //Load DrawNumberScene
        AudioController.instance.PlayButtonMusic();
        SceneManager.LoadScene(4);
    }

    public void ChangeToCalculateScene()
    {
        //Load CalculateScene
        AudioController.instance.PlayButtonMusic();
        SceneManager.LoadScene(1);
    }

    public void ChangeToIdentifyScene()
    {
        //Load IdentifyScene
        //SceneManager.LoadScene(2);
    }

    public void ChangeToMatchObjectScene()
    {
        //Load MatchObjectScene
        AudioController.instance.PlayButtonMusic();
        SceneManager.LoadScene(5);
    }
    #endregion

    #region SettingPanel's related methods
    public void MusicButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
        if (sprMusicButton.sprite2D.name.Equals("speaker"))
        {
            AudioController.instance.TurnOffSound();
            sprMusicButton.sprite2D = sprDisableButton;
        }
        else
        {
            AudioController.instance.TurnOnSound();
            sprMusicButton.sprite2D = sprNormalButton;
        }  
    }

    public void SoundEffectButton_Click()
    {

    }

    public void AboutButton_Click()
    {
        AudioController.instance.PlayButtonMusic();
    }
    //public void SettingButton_Click()
    //{
    //    StartCoroutine(SettingPanelAnimation());
    //}

    //IEnumerator SettingPanelAnimation()
    //{
    //    //Debug.Log("animation");
    //    if(settingPanelPosition.localPosition.y == -40)
    //    {
    //        //Debug.Log("if loop");
    //        while (settingPanelPosition.localPosition.y != 200)
    //        {
                
    //            //Debug.Log("230");
    //            Vector2 newPos = new Vector2(settingPanelPosition.localPosition.x, settingPanelPosition.localPosition.y + 30);

    //            settingPanelPosition.localPosition = newPos;

    //            yield return new WaitForSeconds(0.1f);
    //        }
    //    }
    //    else
    //    {
    //        while (settingPanelPosition.localPosition.y != -40)
    //        {
    //            yield return new WaitForSeconds(0.1f);
    //            //Debug.Log("-20");
    //            Vector2 newPos = new Vector2(settingPanelPosition.localPosition.x, settingPanelPosition.localPosition.y - 30);

    //            settingPanelPosition.localPosition = newPos;
    //        }
    //    }
        
    //    yield break;
    //}
    #endregion
}
