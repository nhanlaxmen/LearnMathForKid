﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    [SerializeField]private UILabel txtShip;
    [SerializeField]private UILabel txtHearts;
    [SerializeField]private UILabel txtPause;
    [SerializeField]private UILabel txtTable;
    [SerializeField]private UILabel txtSkipButton;
    [SerializeField]private UILabel txtDraw;

	// Use this for initialization
	void Start ()
    {
        txtPause.text = "Tạm  dừng  màn  chơi";
        txtHearts.text = "Số  mạng";
        txtSkipButton.text = "Đã  hiểu";
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
		Time.timeScale = 0;
        switch(sceneIndex)
        {
            case 1:
                {
                    //Play Calculate tutorial
					CalculatorTutorial();
                    break;
                }
            case 2:
                {
                    //Play CountNumber tutorial
                    CountNumberTutorial();
                    break;
                }
            case 5:
                {
                    //Play Puzzle tutorial
					PuzzleTutorial ();
                    break;
                }
            default:
                {
                    //Play DrawNumber and DrawShape tutorial
                    DrawTutorial();
                    break;
                }
        }
	}

	void CalculatorTutorial(){
		txtShip.text = "Câu  hỏi  cần  trả  lời";
		txtTable.text = "Chọn  Đáp  án";
	}

	void PuzzleTutorial(){
		txtShip.text = "Mảnh  ghép";
		txtTable.text = "Hình  ảnh  cần  ghép";
	}

    void CountNumberTutorial()
    {
        //Set up tutorial for scene Count Number
        txtShip.text = "Số  lượng  vật  thể  cần  bắn";
        txtTable.text = "Số  lượng  vật  thể  đã  bắn  được";
    }

    void DrawTutorial()
    {
        //Set up tutorial for scene DrawNumber and DrawShape
        txtDraw.text = "Vùng  vẽ";
    }

	public void Understood(){
        AudioController.instance.StopAudio();
        AudioController.instance.PlayButtonMusic();
		Time.timeScale = 1;
		gameObject.SetActive (false);
	}
}
