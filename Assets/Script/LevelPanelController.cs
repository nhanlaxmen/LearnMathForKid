﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class LevelPanelController : MonoBehaviour {

	[SerializeField] Transform srollView ;
	private String btnSpriteName = "Button_37";
	private String starSpriteName = "btnStar1";
	bool nextLvl;

	int maxLevel;

    //private LevelPanelController lvlControlScript;
    [SerializeField]private GameObject countingController; //CountingGameController
    [SerializeField]private GameObject drawShapeController;
    [SerializeField]private GameObject writeNumberController;
    [SerializeField]private GameObject[] numberPrefab;
    [SerializeField]private GameObject[] shapes;

	[SerializeField]private GameObject tutorialPanel;

    private string key;

    public GameObject player; //player prefab in CountNumberScene
    public GameObject sprLevelPrefab; //Reference of the Prefab of Button Level
    public GameObject sprLevel; //Reference of Level Prefab for Draw Number and Draw Image



	void Start () {
		
        int index = SceneManager.GetActiveScene().buildIndex;

        switch(index)
        {
            case 2:
                {
                    key = "count";
                    break;
                }
            case 3:
                {
                    key = "drawN";
                    break;
                }
            case 4:
                {
                    key = "drawI";
                    break;
                }
            case 5:
                {
                    key = "puzzle";
                    break;
                }
            default:
                {
                    key = "cal";
                    break;
                }
        }
		maxLevel = 10;

        player = GameObject.FindGameObjectWithTag("Player");
        //countingController = GameObject.FindGameObjectWithTag("GameController");
        //lvlControlScript = countingController.GetComponent<LevelPanelController>();
        if(Gamepublic.selectedLvl == 0)
        {
            LoadLevel();
            srollView.GetComponent<UIGrid>().enabled = true;
            nextLvl = false;

            if (player != null)
                player.SetActive(false);

            AudioController.instance.StopBackgroundMusic();
        }
        else
        {
            switch (key)
            {
                case "count":
                    {
                        StartCounting();
                        break;
                    }
                case "drawN":
                    {
                        StartDrawNumber();
                        break;
                    }
                case "drawI":
                    {
                        StartDrawImage();
                        break;
                    }
                case "puzzle":
                    {
                        StartMatching();
                        break;
                    }
                default:
                    {
                        StartCalculate();
                        break;
                    }
            }
            NGUITools.Destroy(this.gameObject);
        }    

 
    }


	void LoadLevel(){
		
		for (int i = 1; i <= maxLevel; i++) {
            GameObject child;
            if(key.Equals("drawN") || key.Equals("drawI") || key.Equals("count"))
            {
                child = (GameObject)Instantiate(sprLevel);
            }
            else
            {
                child = Instantiate(Resources.Load("Prefabs/child") as GameObject);
            }
			
			child.transform.SetParent(srollView);
			child.transform.localScale = Vector3.one;
			child.transform.localPosition = Vector3.zero;
			child.name = i.ToString ();
			GameObject star = child.transform.FindChild ("star").gameObject;
			if(PlayerPrefs.HasKey (key  + i.ToString ()) == true){
				EventDelegate.Set (child.GetComponent<UIButton> ().onClick,onClickLevel);
				child.transform.GetComponentInChildren<UILabel> ().text = i.ToString ();
				int numberStar = PlayerPrefs.GetInt (key + i.ToString ());
				for(int j=0 ; j < numberStar ; j++){
					star.transform.GetChild (j).GetComponent<UISprite> ().spriteName = starSpriteName;
				}
			}else{

				if(!nextLvl){
					nextLvl = true;
					EventDelegate.Set (child.GetComponent<UIButton> ().onClick,onClickLevel);
					child.transform.GetComponentInChildren<UILabel> ().text = i.ToString ();
				}else{
					child.GetComponent<UISprite> ().spriteName = btnSpriteName;

					star.SetActive (false);
				}

			}
		}
	}

	void onClickLevel(){
		Gamepublic.selectedLvl = int.Parse(UIButton.current.name) ;
		switch(key)
        {
            case "count":
                {
                    StartCounting();
                    break;
                }
            case "drawN":
                {
                    StartDrawNumber();
                    break;
                }
            case "drawI":
                {
                    StartDrawImage();
                    break;
                }
            case "puzzle":
                {
                    StartMatching();
                    break;
                }
            default:
                {
                    StartCalculate();
                    break;
                }
        }
		NGUITools.Destroy (this.gameObject);
	}

    //Khoi dong scene Phep tinh
    void StartCalculate()
    {
        //Start calculate

        GameObject gameplayPanel = Instantiate(Resources.Load("Prefabs/GameplayPanel") as GameObject);
        gameplayPanel.transform.SetParent(transform.parent);
        gameplayPanel.transform.localScale = Vector3.one;
        gameplayPanel.transform.localPosition = Vector3.zero;
        gameplayPanel.GetComponent<GameplayController>().StartGame();
		if(Gamepublic.selectedLvl == 1)
			tutorialPanel.SetActive (true);

    }

    void StartCounting()
    {
        //Start counting game
        countingController.SetActive(true);
        //lvlControlScript.enabled = true;
        player.SetActive(true);
        
    }

    void StartDrawNumber()
    {
        writeNumberController.SetActive(true);
        //Start draw number
        player.SetActive(true);
        GameObject number = (GameObject)Instantiate(numberPrefab[Gamepublic.selectedLvl - 1]);
        number.transform.SetParent(player.transform);
        number.transform.localPosition = Vector3.zero;
        number.transform.localScale = Vector3.one;
    }

    void StartDrawImage()
    {
        drawShapeController.SetActive(true);
        //Start draw image
        player.SetActive(true);
        GameObject shape = (GameObject)Instantiate(shapes[Gamepublic.selectedLvl - 1]);
        shape.transform.SetParent(player.transform);
        shape.transform.localPosition = Vector3.zero;
        shape.transform.localScale = Vector3.one;
    }

    void StartMatching()
    {
        //Start matching
		GameObject gameplayPanel = Instantiate(Resources.Load("Prefabs/PuzzleGame/PuzzleGameplay") as GameObject);
		gameplayPanel.transform.SetParent(transform.parent);
		gameplayPanel.transform.localScale = Vector3.one;
		gameplayPanel.transform.localPosition = Vector3.zero;
		gameplayPanel.GetComponent<PuzzleGameplayController>().StartGame();

		if(Gamepublic.selectedLvl == 1)
			tutorialPanel.SetActive (true);
    }

	public void btnHome(){
        // back to Main Menu
        AudioController.instance.PlayButtonMusic();
        Gamepublic.isFirstRun = false;
        SceneManager.LoadScene(0);
	}

}
