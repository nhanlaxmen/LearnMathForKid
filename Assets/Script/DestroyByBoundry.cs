﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundry : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Missile") || other.tag.Equals("block"))
        {
            Destroy(other.gameObject);
        }
    }
}
