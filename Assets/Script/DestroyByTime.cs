﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour
{
    private float waitTime;

	// Use this for initialization
	void Start ()
    {
        waitTime = 1.0f;
        StartCoroutine(WaitTimeBeforeDestroy());
	}

    IEnumerator WaitTimeBeforeDestroy()
    {
        yield return new WaitForSeconds(waitTime);

        NGUITools.Destroy(gameObject);
        //Destroy(gameObject);
    }
}
