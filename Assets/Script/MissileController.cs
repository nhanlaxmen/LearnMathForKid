﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissileController : MonoBehaviour
{
    [SerializeField]private Transform missileSpawnLocation;
    [SerializeField]private GameObject missilePrefab;

    private float missileSpeed;
    private float fireRate;
    private float nextFire;

    public Transform MissileSpawnLocation
    {
        get
        {
            return missileSpawnLocation;
        }
        set
        {
            missileSpawnLocation = value;
        }
    }

	// Use this for initialization
	void Start ()
    {      
        missileSpeed = 1.0f;
        fireRate = 3f;
        nextFire = 1f;
	}

    public void OnClick()
    {
        //Debug.Log("Test on mouse down");
        if (Time.time > nextFire)
        {
            AudioController.instance.PlayMissleSound();
            //Instantiate missile when tap on object
            //Debug.Log("Test spawn missile");
            GameObject missile = (GameObject)Instantiate(missilePrefab);
            missile.transform.SetParent(GameObject.FindGameObjectWithTag("Camera").transform);
            missile.transform.localPosition = missileSpawnLocation.localPosition;
            missile.transform.localScale = Vector3.one;
            missile.GetComponent<Rigidbody2D>().velocity = missile.transform.forward * missileSpeed;
            nextFire = Time.time + fireRate;

            //Debug.Log("Spawn missile success");
            TweenTransform navigateScript = missile.GetComponent<TweenTransform>();
            navigateScript.from = missile.transform;
            navigateScript.to = gameObject.transform;
            //navigateScript.delay = 1f;
        }
    }
}
